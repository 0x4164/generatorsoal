-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2019 at 11:08 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gensoal`
--

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id` int(8) NOT NULL,
  `content` text NOT NULL DEFAULT '-',
  `o1` varchar(255) NOT NULL DEFAULT '-',
  `o2` varchar(255) NOT NULL DEFAULT '-',
  `o3` varchar(255) NOT NULL DEFAULT '-',
  `o4` varchar(255) NOT NULL DEFAULT '-',
  `o5` varchar(255) NOT NULL DEFAULT '-',
  `difficulty` enum('1','2','3','4','5') NOT NULL DEFAULT '1',
  `topic` int(5) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id`, `content`, `o1`, `o2`, `o3`, `o4`, `o5`, `difficulty`, `topic`) VALUES
(1, 'Sempurnakan potongan ayat QS. Ali- Imron ayat 190 di bawah ini !', '-', '-', '-', '-', '-', '2', 1),
(2, 'Bacalah baik- baik potongan QS. Ali Imron ayat 190 di bawah ini !\r\n\r\nTerjemahan yang tepat potongan ayat di atas adalah…', 'Di dalam penciptaan langit dan bumi', 'Kekuasaan Allah terdapat di langit dan di bumi', 'Sesungguhnya dalam penciptaan langit dan bumi', 'Sesungguhnya apa apa yang ada di bumi dan langit', 'Orang-orang yang memikirkan kekuasaan Allah baik di langit maupun di bumi', '1', 1),
(3, 'Perhatikan baik- baik potongan ayat di bawah ini ', 'Ghunnah, mad layyin, idzhar syafawi', 'Idgham bighunnah, mad layyin, idzhar Qomariyah', 'Idgham bighunnah, Mad Thabi`I, Idgham syamsiyah', 'Ghunnah, Idzhar Qomariyah, Idgham syamsiyah', 'Ghunnah, Idgham syamsiyah, Idzhar Qomariyah', '2', 1),
(5, 'Sebuah sel di dalam tubuh manusia yang sangat kecil mempunyai system tersendiri yang sangat teratur, yang tidak mungkin semua itu terjadi begitu saja. Apalagi sesuatu yang sangat besar dan sangat hebat seperti penciptaan langit dan bumi, dan bergantinya malam dan siang, itu semua tidak mungkin terjadi secara kebetulan, itu semua ada yang mengaturnya. Menurut anda tindakan mana yang merupakan contoh perilaku berfikir kritis dari pernyataan di atas ?', 'Manusia cukup menerima hasil ciptaan Allah', 'Manusia tidak perlu memikirkan hasil ciptaan-Nya', 'Manusia cukup pasrah saja menerima ciptan_nya', 'Manusia harus berfikir atas kebesaran-Nya', 'Manusia cukup merenungkan hasil ciptaan-Nya', '1', 1),
(6, 'Perhatikan potongan ayat dari QS. Ali- Imron ayat 191 dibawah ini !\r\n\r\nArti kata /mufrodat pada potongan ayat diatas yang bergaris bawah adalah…', 'Allah akan meninggikan derajat orang-orang yang suka hadir di dalam majelis', 'Orang-orang yang mengingat Allah sambil berdiri, duduk dan berbaring', 'Dan orang-orang yang memikirkan penciptaan langit dan bumi', 'Allah memerintahkan manusia untuk memikirkan ciptaannya', 'Allah menciptakan alam semesta ini dengan tidak sia-sia', '1', 1),
(7, 'Di dalam potongan  Q.S. Ali Imron ayat 191 yang berbunyi :\r\n\r\nMenurut ilmu tajwid pada  ( yang bergaris bawah ) terdapat bacaan…', 'Mad thobi`I, qolqolah kubro, ikhfa`', 'Mad thobi`I, qolqolah sugro, ikhfa`', 'Mad thobi`I, idzhar, qolqolah', 'Qolqolah sugro, idzhar, qolqolah sugro', 'Qolqolah kubro, ikhfa`, qolqolah sughro', '1', 1),
(8, 'Dalam  QS. Ali Imran ayat 191 llah menerangkan cara mendekatkan diri kepada Allah dengan mengingat Allah sambil berdiri atau duduk atau dalam keadaan berbaring. Yang demikian itu di namakan…', 'Bersedekah', 'Berinfaq', 'Berzikir', 'Berdoa', 'Beribadah', '1', 1),
(9, 'Orang yang beriman senantiasa merenungkan ciptaan Allah kemudian menangkap hukum-hukum yang terdapat di alam semesta, perilaku demikian itu di namakan…', 'Beribadah', 'Berdzikir', 'Bersyukur', 'Bertasyakkur', 'Bertafakkur', '1', 1),
(10, 'Perhatikan firman Allah dalam surat Toha ayat 15 berikut \r\nInti dari ayat diatas adalah…', 'Gambaran tentang tahap-tahap menuju akhirat', 'Peristiwa dahsyat pada hari kiamat', 'Balasan Allah berupa surge bagi orang beriman', 'Rahasia kapan datangnya hari kiamat', 'Gambaran kehidupan manusia di akhirat', '1', 1),
(11, 'Suatu hari dimana seluruh makhluk hidup akan dibangkitkan dari kuburnya yang ditandai dengan tiupan terompet dari malaikat isrofil, kejadian tersebut dinamakan…', 'Yaumul hisab', 'Yaumul  ba`ats', 'Yaumul  Jaza`', 'Yaumul mizan', 'Yaumul hasyr', '1', 1),
(12, 'Potongan ayat yang mempunyai arti “ bumi diguncangkan dengan guncangan yang hebat ” adalah...', '-', '-', '-', '-', '-', '1', 1),
(13, 'Apabila hari akhir sudah datang, amal manusia akan dihitung meskipun sebesar Zarrah. Lafal yang sesuai dengan konteks ayat tersebut adalah...', '-', '-', '-', '-', '-', '1', 1),
(14, 'Perhatikan terjemahan QS. Hud ayat 108 berikut : \" Adapun orang- orang yang berbahagia, maka tempatnya di dalam surga, mereka kekal di dalamnya selama ada langit dan bumi kecuali jika tuhanmu menghendaki yang lain debagai karunia yang tiada putus-putusnya. Tempat tersebut adalah…', 'Jannatul Ma`wa', 'Jannatul Firdaus', 'Darul Muqomah ', 'Jannatun Na`im', 'Darus Salam', '1', 1),
(15, 'Ketika Allah swt memperlihatkan semua amalan akhirat, segala dosa besar dan kecil dihitung dengan seksama dan teliti. Ketika amalan mereka dihitung, anggota tubuh mereka ikut menjadi saksi. Seperti dijelaskan dalam Firman Allah surat An-Nur ayat 24 yang artinya : \" Pada hari itu lidah, tangan, dan kaki masing-masing menjadi saksi atas perbuatan yang telah mereka kerjakan \". Proses pengadilan akhirat di atas adalah termasuk tahapan… ', 'Yaumul Jaza`', 'Yaumul mizan', 'Yaumul hisab', 'Yaumul akhir', 'Yaumul Ba`ats', '1', 1),
(16, 'Setelah hancurya alam semesta,manusia akan dibangkitkan kembali kemudian dikumpulkan disuatu tempat selama 50.000 tahun yang disebut: ', 'Yaumul Hisab', 'Yaumul Fashl', 'Yaumul ba’tsi', 'Yaumul mahsyar', 'Yaumul jaza’', '1', 1),
(17, 'Setelah kehancuran alam semesta beserta isinya, setiap makhluk akan mengalami proses tahapan sebagai berikut :', 'Yaumul akhir, yaumul ba`ats, yaumul hasyr, yaumul jaza`', 'Yaumul ba`ats, yaumul hasyr, yaumul hisab, yaumulmizan, yaumul jaza`', 'Yaumul khasyr, yaumul ba`ats, yaumul mizan, yaumul hisab, yaumul jaza`', 'Yaumul ba`ats, yaumul hisab, yaumul miza, yaumul akhir, yaumul jaza`', 'Yaumul kiyamah, yaumulba`ats, yaumul hisab, yaumul mizan, yaumul jaza`', '1', 1),
(18, 'Disebutkan dalam salah satu hadits, bahwa amalan yang paling pertama dihisab pada hari \r\n kiamat adalah sholatnya, jika sholatnya diterima maka diterimalah amal-amal yang lain, jika sholatnya ditolak, maka ditolaklah amalan-amalan yang lain. Dari hadits tersebut dapat diambil pelajaran bahwa salah satu pencerminan keimanan kepada hari akhir adalah…', 'Senantiasa bertaqwa kepada Allah', 'Disiplin dalam melaksanakan sholat lima waktu dan ibadah-ibadah yang lain', 'Menyantuni, memelihara, mengasuh dan mendidik anak yatim', 'Berperilaku baik terhadap tetangga, menghormati tamu dan bertutur kata yg baik', 'Mencintai dan menyayangi fakir miskin', '1', 1),
(19, 'Pada tahapan tanya jawab, pada hari kiamat seseorang tidak akan luput dari 4 pertanyaan. Pertanyaan yang dimaksud adalah diseputar...', 'umur, ilmu, harta dan tubuhnya', 'waktu, umur, harta dan ilmunya', 'waktu, harta, ilmu dan kedudukan', 'harta, tahta, ilmu dan keluarganya', 'harta, tahta, istri dan keluarganya', '1', 1),
(20, 'Pada saat manusia memasuki tahap dikumpulkannya di padang mahsyar, terjadi kebingungan yang luar biasa. Masing-masing orang mencari pertolongan/ syafaatkepada para Nabi, Maka yang dapat memberi syafaat adalah Nabi Muhammad beliau berdoa yang isinya…', 'Permohonan Nabi agar umatnya diampuni dosa-dosanya', 'Permohonan Nabi agar umatnya segera masuk surga', 'Permohonan Nabi agar semua manusiadibebaskan dari api neraka', 'Permohonan Nabi agar Yaumul Hisab dan Yaumul Mizan segera dilaksanakan', 'Permohonan Nabi agar umatnya segera dibebaskan dari api neraka dan dimasukkan ke surga', '1', 1),
(21, 'Perhatikan1stilah-istilah  di bawah ini !\r\nDari table diatas,  pasangan yang sesuai dengan  artinya adalah…', 'Nomor 1, 2, dan 3', 'Nomor 2, 3, dan 4', 'Nomor 3, 4, dan 5', 'nomor 1, 2, dan 4', 'nomor 1, 3, dan 5', '1', 1),
(22, 'Surga yang dibawahnya telah mengalir sungai-sungai yang semuanya disediakan kepada umat yang beriman dan bertakwa adalah…', 'Surga Firdaus', 'Darul Muqomah', 'Jannatun Na`im', 'Jannatul ma`wa', 'Surga Adn', '1', 1),
(23, 'Neraka yang disediakan untuk orang-orang yang durhaka kepada Allah swt yang di dalamnya terdapat api yang akan membakar manusia dan mengoyak-oyak kulitnya disebut...', 'Neraka Sa`ir', 'Neraka hutomah', 'Neraka jahim', 'Neraka Saqor', 'Neraka Jahannam', '1', 1),
(24, 'Berikut ini yang tidak termasuk perilaku yang merupakan cerminan keimanan kepada hari kiamat adalah :', 'senantiasa beriman dan beramal sholeh.', 'disiplin dalam melaksanakan sholat..', 'menyantuni dan memelihara anak yatim.', 'memenuhi segala permintaan tetangga.', 'berkata yang baik-baik saja atau diam', '1', 1),
(25, 'Banyak sekali hikmah beriman kepada hari akhir untuk kehidupan di dunia, diantaranya ;” member dorongan untuk membiasakan diri dengan sikap dan perilaku terpuji dan menjauhkan diri dari sikap dan perilaku tercela “ Hal ini dikarenakan dalam iman kepada hari akhirat tersebut adanya keyakinan bahwa…', 'Surga adalah tempat yang menyenangkan', 'Neraka adalah tempat yang sangat pedih', 'Kelak segala amal akan diperlihatkan', 'Kita semua akan dibangkitkan', 'Akan nada hari meniti shirat', '1', 1),
(26, 'Menurut pengertian bahasa, arti qodho adalah menentukan, sementara arti qadar secara bahasa adalah…', 'Memberi ketentuan', 'Sesuatu yang pasti', 'Sebuah keputusan ', 'Adanya ketetapan', 'Seluruh rencana', '1', 1),
(27, 'Takdir terbagi dua , yaitu takdir Muallaq dan takdir Mubram, yang di maksud takdir Mubram adalah…', 'Ketentuan Allah swt yang akan terjadi', 'Ketentuan Allah swt yang mungkin terjadi', 'Ketentuan Allah yang belum pasti terjadi', 'Ketentuan Allah swt , tergantung ikhtiar manusia', 'Ketentuan Allah yang tidak dapat diubah', '1', 1),
(28, 'Takdir terbagi dua , yaitu takdir Muallaq dan takdir Mubram, yang di maksud takdir Mubram adalah…', 'Ketentuan Allah swt yang akan terjadi', 'Ketentuan Allah swt yang mungkin terjadi', 'Ketentuan Allah yang belum pasti terjadi', 'Ketentuan Allah swt , tergantung ikhtiar manusia', 'Ketentuan Allah yang tidak dapat diubah', '1', 1),
(29, 'Sunnatullah merupakan hukum-hukum Allah swt yang ada di alam semesta. Pernyataan dibawah ini yang bukan merupakan contoh dari sunnatullah adalah :', 'Air yang dapat mendidih apabia dipanaskan hingga 100 derajat celcius', 'Bulan bersinar pada malam hari danmatahari bersinar pada siang hari', 'Setiap benda yang dijatuhkan atmosfir akan jatuh ke bumi', 'Matahari terbit dari arah timur dan terbenam di sebelah barat', 'Planet – planet yang senantiasa berputar pada porosnya', '1', 1),
(30, 'Menghadapi ujian akhir, ahmad berusaha belajar dengan giat dan maksimal, tidak lupa juga berdoa setelah selesai shalat fardhu. Kegiatan yang dilakukan Ahmad di namakan :', 'Muhasabah diri', 'Tawakkal kepada Allah', 'Ikhtiar di sertai doa', 'Qodar Allah', 'Qodho Allah', '1', 1),
(31, 'Setelah berusaha, Aisyah selalu berdoa mohon yang terbaik, Anita selalu yakin akan keberhasilannya, Alia selalu memasrahkan diri kepada Allah swt atas hasil usahanya, lain lagi dengan Anisa yang malas berdoa dan berusaha.Melalui diskripsi tersebut, yang sudah mengamalkan perilaku tawakkal kepada Allah adalah  :', 'Alia dan Annisa', 'Aisyah dan Anita', 'Anisa dan Aisyah', 'Aisyah dan Alia', 'Anita dan Alia', '1', 1),
(32, 'Beriman kepada qodho dan qodar harus tampak pada perilaku kehidupan orang beriman, salah satu contohnya adalah …', 'Rina sangat pesimis menghadapi kehidupan yang kian hari kian semakin sulit', 'Lukman tidak pernah putus asa apabila menemui kegagalan dalam berusaha', 'Adi selalu berpendapat apapun yang dilakukan manusia pasti berhasil', 'Yulia malas berdoa karena banyak keinginannya yang tidak tercapai', 'Hendra merasa tidak ada gunanya belajar, karena nilai ulangannya jelek', '1', 1),
(33, 'Perhatikan pernyataan- pernyataan berikut !\r\n1)  Manusia bebas menentukan langkah kehidupannya\r\n2)  Hidupnya selalu berorientas pada materi\r\n3)  Sesuatu yang terjadi di alam sesuai dengan sunnatullah\r\n4)  Tidak melakukan usaha karena semua sudah di tentukan\r\n5)  Sadar bahwa dalam hidup manusia dibatasi oleh aturan\r\n6)  Agar lebih fokus dalam berusaha, jangan lupa berdoa\r\n\r\nPernyataan tersebut yang tidak termasuk tanda-tanda beriman kepada qodho dan qodar\r\n terdapat dalam nomor :', '1), 2), dan 4) ', '1), 4), dan 6)', '1), 5), dan 6)', '2), 3) dan 5)', '2), 4) dan 5)', '1', 1),
(34, 'Bukti seseorang beriman adalah meyakini segala sesuatu yang terjadi di alam semesta merupakan kehendak dan ketentuan Allah swt. Berikut yang tidak termasuk hikmah beriman kepada qodho dan qodar adalah …', 'Menumbuhkan berbagai sikap dan perilaku terpuji', 'Dapat menghilangkan sikap dan perilku yang tercela', 'Menerima apa yang terjadi pada dirinya tanpa ada usaha', 'Dapat meningkatkan keimanan dan ketakwaan kepada  Allah', 'Meningkatkan keimanan pada adanya peristiwa hari akhir', '1', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
