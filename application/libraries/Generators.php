<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

use \PhpOffice\PhpWord\PhpWord;
use \PhpOffice\PhpWord\TemplateProcessor;

class Generators{
	protected $ci;

	public function __construct(){
		$this->ci=&get_instance();
		$this->ci->load->model('msoal');
	}

	public function preout($v=""){
		echo "<pre>";
		var_dump($v);
		echo "</pre>";
	}
	
	//0 algorithm
	/*
	 add pool formula (algorithm)
	 by random - just select random soal
	 by difficulty (1,2,3,4,5) 1 newbie - 5 expert
	 
	 by subject - select
	 
	 by composition of difficulty rate 
	*/
    public function generate(){
		// $a=[1,2,3];
		
		$q=$this->ci->msoal->getSoal()->result();
		// foreach()
        $ret=$q;
        return $ret;
	}

	//return @array
	public function generateTemplateArr(){
		$ret=[];
		$arr=range(1,50);
		$arr2=['A','B','C','D','E'];
		foreach($arr as $a){
			$s=[null,null];
			$j=[];
			foreach($arr2 as $a2){
				$j[]="s".$a."j".$a2;
			}
			$s[0]="s".$a;
			$s[1]=$j;
			$ret[]=$s;
		}
		return $ret;
	}

	//return @str
	public function generateTemplateStr(){
		$out="";
		$arr=range(1,50);
		$arr2=['A','B','C','D','E'];
		foreach($arr as $a){
			$s="\${s".$a."}\n";
			foreach($arr2 as $a2){
				$s.=$a2."\t\${s".$a."j".$a2."}\n";
			}
			$out.=$s;
		}
		return $out;
	}
	/*
	${s1}
A	${s1j1}
B	${s1j2}
C	${s1j3}
D	${s1j4}
E	${s1j5}

	[{"id":"1","content":"s1","difficulty":"1","topic":"1"},
	{"id":"2","content":"s2","difficulty":"2","topic":"1"},
	{"id":"3","content":"s3","difficulty":"2","topic":"1"}]
	 */
	//input dari result()
	public function formatSoal($phpWord=null,$section=null,$arr=[]){
		// $ret="";
		$n=1;
		$fontStyle = new \PhpOffice\PhpWord\Style\Font();
		// $fontStyle->setBold(false);
		$fontStyle->setName('Times New Roman');
		$fontStyle->setSize(12);

		$phpWord->addNumberingStyle(
			'multilevel',
			array(
				'type' => 'multilevel',
				'levels' => array(
					array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
					array('format' => 'upperLetter', 'text' => '%2.', 'left' => 720, 'hanging' => 360, 'tabPos' => 720),
				)
			)
		);
		// $myTextElement = $section->addText('"Believe you can and you\'re halfway there." (Theodor Roosevelt)');
		// $myTextElement->setFontStyle($fontStyle);
		foreach($arr as $a){
			$soal="";
			$soal.=$a->content;
			// coba add text
			// $soaltext=$section->addText($soal);
			// $soaltext->setFontStyle($fontStyle);
			
			// coba add list
			// $section->addListItem($text, [$depth], [$fontStyle], [$listStyle], [$paragraphStyle]);
			$section->addListItem($soal, 0, $fontStyle, 'multilevel');
			$section->addListItem($a->o1, 1, $fontStyle, 'multilevel');
			$section->addListItem($a->o2, 1, $fontStyle, 'multilevel');
			$section->addListItem($a->o3, 1, $fontStyle, 'multilevel');
			$section->addListItem($a->o4, 1, $fontStyle, 'multilevel');
			$section->addListItem($a->o5, 1, $fontStyle, 'multilevel');
			$n++;
		}

		return $section;
	}

	/*
	s=soal
	j=jawaban
	${s1}
A	${s1j1}
B	${s1j2}
C	${s1j3}
D	${s1j4}
E	${s1j5}
	*/
    
    public function generateFromTemplate($templatename="docs/smaba1.docm",$output="docs/outtemp.docm"){
		$templateProcessor = new TemplateProcessor($templatename);
		$nsoal=30;
		$gen=$this->generate();
		$fmt=$this->generateTemplateArr();
		$n=0;
		foreach($fmt as $f){
			if($n>=$nsoal-1){break;}
			$soal=$gen[$n];
			$templateProcessor->setValue($f[0], $soal->content);
			$templateProcessor->setValue(
				$f[1], 
				[
					$soal->o1,
					$soal->o2,
					$soal->o3,
					$soal->o4,
					$soal->o5,
				]
			);
			$n++;
		}
		$templateProcessor->saveAs($output);
	}

    public function generateDocx($fname="docs/a.docx"){
        $phpWord = new PhpWord();
		/* Note: any element you append to a document must reside inside of a Section. */

		// Adding an empty Section to the document...
		$section = $phpWord->addSection();
		// Adding Text element to the Section having font styled by default...
		// $section->addText(
		// 	$text
		// );
		$gen=$this->generate();
		$this->formatSoal($phpWord,$section,$gen);

		// $section->addListItem('List Item I', 0, null, 'multilevel');
		// $section->addListItem('List Item I.a', 1, null, 'multilevel');
		// $section->addListItem('List Item I.b', 1, null, 'multilevel');
		// $section->addListItem('List Item II', 0, null, 'multilevel');

		// Saving the document as OOXML file...
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$objWriter->save($fname);
	}
}