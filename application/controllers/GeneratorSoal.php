<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// use \Data;

class Data{

}

class GeneratorSoal extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('msoal');
		$this->load->library('generators');
	}
	
	public function index()
	{
		$fname='docs/out1.docx';
		$fromtemp="docs/smaba1.docm";
		$output="docs/outtemp.docm";
		// $this->generators->generateDocx($fname);
		$this->generators->generateFromTemplate($fromtemp,$output);
		$fname=$fromtemp;
		$file=new Data();
		$file->name=$fname;

		$data['file']=$file;
		$this->load->view('vgenerator',$data);
	}

	public function generateTemplate(){
		$str=$this->generators->generateTemplate();
		$fname="docs/out.txt";
		$f=fopen($fname,'w');
		fwrite($f,$str);
		fclose($f);
		echo "out to ".$fname;
	}
}
