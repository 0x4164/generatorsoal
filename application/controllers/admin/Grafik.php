<?php

ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

defined('BASEPATH') OR exit('No direct script access allowed');

class Data{
    
}

class Grafik extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Grafik_model");
        $this->load->model("Gerak_model");
        $this->load->library('form_validation');
    }

    public function index($date)
    {
        $data["g_perwaktu"] = $this->Product_model->getAll();
        $data['tgl'] = $date;
        $this->load->view("admin/product/g_perwaktu2", $data);
    }

    public function getPerHari()
    {
        // $data["g_perhari"] = $this->Grafik_model->getPerDay();
        // $data['tgl'] = $date;
        // $data["perWaktu"] =$this->Grafik_model->getPerHour()
        $this->load->view("admin/product/g_perhari");
    }

    public function getPerJam($date='2019-08-05')
    {
        $data["g_perwaktu"] = $this->Product_model->getAll();
        $data['tgl'] = $date;
        $dir = "data/";
        $files = scandir($dir);
        $data['ls']=$this->getDir();
        $data['statistik']=$this->getStatistik($date);
        $this->load->view("admin/product/g_perwaktu",$data);
    }

    public function preout($v=""){
        echo "<pre>";
        var_dump($v);
        echo "</pre>";
    }

    public function getDir($dir='data/'){
        $files = scandir($dir);
        // print_r($files);
        //gen label tanggal
        
        $arrayTanggal=[];
        $at=0;
        foreach($files as $f){
            if($at<2){
                $at++;
                continue;
            }
            $st=substr($f,0,4)."-".substr($f,4,2)."-".substr($f,6,2);
            // echo $st;
            $arrayTanggal[]=$st;
            $at++;
        }
        return $arrayTanggal;
    }

    public function getDbDate(){
        //gen label tanggal
        
        $arrayTanggal=[];
        $at=0;
        $gerak = $this->Gerak_model->getDistinctDate();
        $gas = $this->Grafik_model->getDistinctDate();
        
        foreach($gerak->result() as $ge){
            $arrayTanggal[]=$ge->gerakdate;
        }

        foreach($gas->result() as $g){
            $arrayTanggal[]=$g->gasdate;
        }
        
        $dir=$this->getDir();
        $arr=array_unique($arrayTanggal);
        $arrayTanggal=[];
        foreach($arr as $e){
            $arrayTanggal[]=$e;
        }

        return $arrayTanggal;
    }

    public function getData(){
        header('Content-Type:text/json');
        $dir = "data/";
        $files = scandir($dir);
        // print_r($files);
        //gen label tanggal
        
        // $arrayTanggal=$this->getDir();
        $arrayTanggal=$this->getDbDate();

        $labels=$arrayTanggal;

        $datasets=[];

        $data=[
            ["Sensor Gas",'rgb(40, 178, 170, 0.2)','rgba(135, 206, 250, 1)'],
            ["Gerakan",'#B0C4DE','#00FFFF']
        ];
        $dc=0;
        foreach($data as $d){
            $singleset=new Data();
            $singleset->label=$d[0];
            $dataarr=[];
            $bgarr=[];
            $borderarr=[];
            // get from db
            //iterate
            foreach($arrayTanggal as $att){
                $jumlah="";
                if($dc==0){
                    //get data gas by tanggal
                    $jumlah = $this->Grafik_model->getDataByTime($att);
                }else{
                    //get data gerak by tanggal
                    $jumlah = $this->Gerak_model->getDataByTime($att);
                }
                $dataarr[]=$jumlah;
                
            }
            foreach($labels as $l){
                $dbarr=[];
                $bgarr[]=$d[1];
                $borderarr[]=$d[2];
            }
            // $dataarr=$dbarr;
            $singleset->data=$dataarr;
            $singleset->backgroundColor=$bgarr;//json_encode();
            $singleset->borderColor=$borderarr;//json_encode($borderarr);
            $singleset->borderWidth=1;
            $datasets[]=$singleset;
            $dc++;
        }

        $ret=new Data();
        $ret->labels=$labels;
        $ret->datasets=$datasets;

        echo json_encode($ret);
    }

    public function getDataWaktu($tanggal='2019-08-05'){
        header('Content-Type:text/json');
        // print_r($files);
        //gen label tanggal
        $tanggal=$this->input->get('tanggal');

        $labels=[
            ['07','08','Pagi'],
            ['11','12','Siang'],
            ['16','17','Sore'],
        ];

        $datasets=[];

        $data=[
            ["Jumlah Kepadatan",'#B0C4DE','#00FFFF'],
            ["Rata-rata Polusi Per Tanggal",'rgb(40, 178, 170, 0.2)','rgba(135, 206, 250, 1)'],
        ];
        $dc=0;
        foreach($data as $d){
            $singleset=new Data();
            $singleset->label=$d[0];
            $dataarr=[];
            $bgarr=[];
            $borderarr=[];
            // get from db
            //iterate
            foreach($labels as $att){
                $jumlah="";
                if($dc==0){
                    //get data gerak by time tgl
                    $jumlah = $this->Gerak_model->getPerHour($att[0], $att[1], $tanggal)[0]['jumlah'];
                }else{
                    //get data gas by time tgl
                    $jumlah = $this->Grafik_model->getPerHour($att[0], $att[1], $tanggal)->rata;
                }
                // $dbarr[]=$jumlah;
                $dataarr[]=$jumlah;
            }
            foreach($labels as $l){
                $dbarr=[];
                $bgarr[]=$d[1];
                $borderarr[]=$d[2];
            }
            // $dataarr=$dbarr;
            $singleset->data=$dataarr;
            $singleset->backgroundColor=$bgarr;//json_encode();
            $singleset->borderColor=$borderarr;//json_encode($borderarr);
            $singleset->borderWidth=1;
            $datasets[]=$singleset;
            $dc++;
        }

        $ret=new Data();
        $ret->labels=$labels;
        $ret->datasets=$datasets;

        echo json_encode($ret);
    }

    public function getStatistik($tanggal='2019-08-05'){
        $tgl=$tanggal;
        $st=[];
        $dgaPagi =$this->Grafik_model->getPerHour('07', '08', $tgl)->rata;
        $dgaSiang =$this->Grafik_model->getPerHour('11', '12', $tgl)->rata;
        $dgaSore =$this->Grafik_model->getPerHour('16', '17', $tgl)->rata;

        $dgePagi =$this->Gerak_model->getPerHour('07', '08', $tgl)[0]['jumlah'];
        $dgeSiang =$this->Gerak_model->getPerHour('11', '12', $tgl)[0]['jumlah'];
        $dgeSore =$this->Gerak_model->getPerHour('16', '17', $tgl)[0]['jumlah'];
        $data=["Nilai",];
        $ret=[];
        foreach($data as $d){
            $da=new Data();
            $da->nama=$d;
            $da->dgaPagi=$dgaPagi?$dgaPagi:0;
            $da->dgaSiang=$dgaSiang?$dgaSiang:0;
            $da->dgaSore=$dgaSore?$dgaSore:0;
            $da->dgePagi=$dgePagi?$dgePagi:0;
            $da->dgeSiang=$dgeSiang?$dgeSiang:0;
            $da->dgeSore=$dgeSore?$dgeSore:0;
            $d=$da;
            $ret[]=$d;
        }
        // $st=$data;
        // var_dump($st);
        return $ret;
    }

    public function getgerak(){
        $this->Gerak_model->truncateData();

        $dir = "data/";
        $files = scandir($dir);
        //print_r($files);

        for($i=0;$i<count($files);$i++)
        {   if(substr($files[$i],0,4)=='2019')
            {
                // echo "Folder = ".$files[$i]." berisi : <br/>";      
                // $files[$i] = $dir."/".$files[$i];
                $file_avi = scandir($dir."/".$files[$i]);
                $no=1;
                for($j=0;$j<count($file_avi);$j++)
                {   if(substr($file_avi[$j],-4)=='.avi')
                    {   
                        $path_file_avi = $files[$i]."/".$file_avi[$j];
                        // masukkan ke db dan buat grafiknya, digabung dgn data mq7
                        
                        // echo $no.". ".$file_avi[$j]." >> ".date("d-m-Y H:i:s",filemtime($path_file_avi))." >> ".filesize($path_file_avi)." byte <br/>"; 
                        //========================================================================================
                        // mecah waktu
                        //========================================================================================

                        $array_name = explode("_", $file_avi[$j]);
                        $waktuText = $array_name[3];

                        $waktuTextArray = str_split($waktuText);
                        $waktu=$waktuTextArray[0].$waktuTextArray[1].":".$waktuTextArray[2].$waktuTextArray[3].":".$waktuTextArray[4].$waktuTextArray[5];
                        
                        $tanggal = $files[$i]." ".$waktu;
                        $nama    = $file_avi[$j];
                        //=======================
         
                        $data = array(
                            'nama' => $nama,
                            'tanggal' => $tanggal,
                            );
                       
                        $this->Gerak_model->input_data($data,'data_gerak');

                        // echo $no.". ".$file_avi[$j]." >> ".date("d-m-Y H:i:s",filemtime($path_file_avi))." >> ".filesize($path_file_avi)." byte <br/>"; 

                        $no++;      
                    }
                }
            }
        }

        echo "Sukses Simpan!";
    }

}

?>
