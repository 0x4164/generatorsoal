<!-- Sidebar -->
<ul class="sidebar navbar-nav">

    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('grafik/2019-08-05') ?>">
          <i class="fa fa-book fa-fw" aria-hidden="true"></i> 
            <span>Grafik Monitoring  Kepadatan & Polusi Per Waktu</span>
        </a>
    </li>


    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('harian') ?>">
            <i class="fa fa-book fa-fw" aria-hidden="true"></i>  
            <span> Grafik Monitoring Kepadatan & Tingkat Polusi Per Hari</span>
        </a>
    </li>
    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('products') ?>">
             <i class="fa fa-book fa-fw" aria-hidden="true"></i>    
            <span> Data Tingkat Polusi </span>
        </a>
    </li>

    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('products') ?>">
             <i class="fa fa-book fa-fw" aria-hidden="true"></i>    
            <span> Data Kepadatan </span>
        </a>
    </li>

</ul>