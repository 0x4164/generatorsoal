<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

  <?php $this->load->view("admin/_partials/navbar.php") ?>
  <div id="wrapper">

    <?php $this->load->view("admin/_partials/sidebar.php") ?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <?php $this->load->view("admin/_partials/breadcrumb.php") ?>

        <!-- Letak Grafik -->

        <h4>Grafik Kepadatan & Tingkat Polusi Per Hari</h4>
        <canvas id="myChart"></canvas>
        <?php
        ini_set('max_execution_time', 0); 
        ini_set('memory_limit','2048M');
        ?>
      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <?php $this->load->view("admin/_partials/footer.php") ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->


  <?php $this->load->view("admin/_partials/scrolltop.php") ?>
  <?php $this->load->view("admin/_partials/modal.php") ?>

  <?php $this->load->view("admin/_partials/js.php") ?>

  <!-- Grafik Chart JS -->
  <script>
    var bg=[
      'rgb(40, 178, 170, 0.2)',
      '#B0C4DE',
    ]

    var border=[
      'rgba(135, 206, 250, 1)',
      '#00FFFF',
    ]

    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart;
    
    function initChart(pctx,pdata){
      myChart = new Chart(
        pctx, {
          type: 'bar',
          data: pdata,
          options: {
              animation:false,
              scales: {
                  yAxes: [{
                      ticks: {
                          // beginAtZero: true
                      }
                  }]
              }
          }
        });
    }

      //ajax
      function getData(){
        console.log("get")
        $.ajax({
            type: 'GET',
            url: baseurl+'index.php/api/getData',
            data: {},
            success: function(data) {
              // json = JSON.parse(data);
              json=data
              console.log(data);
              // myChart.data=json
              initChart(ctx,data)
              console.log(json)
            }
        });
      }
      getData()

      setInterval(getData, 10000);

  </script>

</body>

</html>


