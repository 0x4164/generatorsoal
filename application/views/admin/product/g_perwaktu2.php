<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

  <?php $this->load->view("admin/_partials/navbar.php") ?>
  <div id="wrapper">

    <?php $this->load->view("admin/_partials/sidebar.php") ?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <?php $this->load->view("admin/_partials/breadcrumb.php") ?>

        <!-- Letak Grafik -->

        <h4>Grafik Monitoring Kepadatan & Tingkat Polusi Per Waktu</h4>
        <?php
            // $dir = "data/";
            // $files = scandir($dir);

            // $z = 0;
            // for ($i=2; $i<count($files) ; $i++) { 
            //     $tglData[$z] = $files[$i];
            //     $z++;
            // }
          $tglData = array('2019-08-05', '2019-08-07', '2019-08-10', '2019-08-12', '2019-08-14', '2019-08-17', '2019-08-19', '2019-08-21', '2019-08-24');
        ?>
        <ul>
          <?php for($d=0; $d<count($tglData); $d++) { ?>
            <li><a href="<?php echo base_url().'index.php/grafik/'.$tglData[$d]; ?>">
              Ke tanggal <?php echo $tglData[$d]; ?>
            </a></li>
          <?php } ?>
        </ul>

        <canvas id="myChart"></canvas>

        <?php $dataPagi =$this->Grafik_model->getPerHour('07', '08', $tgl) ?>
        <?php $dataSiang =$this->Grafik_model->getPerHour('11', '12', $tgl) ?>
        <?php $dataSore =$this->Grafik_model->getPerHour('16', '17', $tgl);
        // echo sizeof($dataPagi);
         ?>

        <?php $d_Pagi =$this->Gerak_model->getDataByTime($tgl, '08') ?>
        <?php $d_Siang =$this->Gerak_model->getDataByTime($tgl, '13') ?>
        <?php $d_Sore =$this->Gerak_model->getDataByTime($tgl, '17') ?>
      </br>
      </br>
        <?php echo "<p><b>Rata rata untuk Tanggal ".$tgl." Jam 07.00 - 08.00 WIB: ".$dataPagi->rata.".</p></b>"; ?>
        <?php echo "<p><b>Rata rata untuk Tanggal ".$tgl." Jam 12.00 - 13.00 WIB: ".$dataSiang->rata.".</p></b>"; ?>
        <?php echo "<p><b>Rata rata untuk Tanggal ".$tgl." Jam 16.00 - 17.00 WIB: ".$dataSore->rata.".</p></b>"; ?>

        <?php echo "<p><b>Rata rata untuk Tanggal ".$tgl." Jam 07.00 - 08.00 WIB: ".$d_Pagi.".</p></b>"; ?>
        <?php echo "<p><b>Rata rata untuk Tanggal ".$tgl." Jam 12.00 - 13.00 WIB: ".$d_Siang.".</p></b>"; ?>
        <?php echo "<p><b>Rata rata untuk Tanggal ".$tgl." Jam 16.00 - 17.00 WIB: ".$d_Sore.".</p></b>"; ?>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <?php $this->load->view("admin/_partials/footer.php") ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->


  <?php $this->load->view("admin/_partials/scrolltop.php") ?>
  <?php $this->load->view("admin/_partials/modal.php") ?>

  <?php $this->load->view("admin/_partials/js.php") ?>

  <!-- Grafik Chart JS -->
  <script>

    var ctx = document.getElementById('myChart').getContext('2d');
    var myLineChart= new Chart(ctx, {
        type: 'bar',
       data: {
            labels: ['Pagi', 'Siang', 'Sore'],
            datasets: [{
                label: 'Rata-rata Polusi Per Tanggal',
                data: [<?php echo $dataPagi->rata.", ".$dataSiang->rata.", ".$dataSore->rata; ?>],
                backgroundColor: [
                    '#DEB886', 
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
            }, 
              {
                label: 'Jumlah Kepadatan',
                data: [<?php echo ($d_Pagi/10).", ".($d_Siang/10).", ".($d_Sore/10); ?>],
                backgroundColor: [
                  '#DC143C'
                ],
                borderColor: [
                  '#00FFFF'
                ],
                borderWidth: 1
              },
              
            ],
          
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
  </script>

</body>

</html>


