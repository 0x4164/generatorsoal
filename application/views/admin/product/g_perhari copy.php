<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

  <?php $this->load->view("admin/_partials/navbar.php") ?>
  <div id="wrapper">

    <?php $this->load->view("admin/_partials/sidebar.php") ?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <?php $this->load->view("admin/_partials/breadcrumb.php") ?>

        <!-- Letak Grafik -->

        <h4>Grafik Kepadatan & Tingkat Polusi Per Hari</h4>
        <?php
          //dinamis
          $dir = "data/";
          $files = scandir($dir);

          $z = 0;
          for ($i=2; $i<count($files) ; $i++) { 
              $tglData[$z] = $files[$i];
              $z++;
          }

          // var_dump($tglData);

          // $tglData = array('2019-08-05', '2019-08-07', '2019-08-10', '2019-08-12', '2019-08-14', '2019-08-17', '2019-08-19', '2019-08-21', '2019-08-24');
        ?>

        <canvas id="myChart"></canvas>

        <?php
          $dataHarian = array();
          $dataTgl = 0;
          for($d=0; $d<count($tglData); $d++) {
            $tgl =  $tglData[$d];
            $dataTgl = $this->Grafik_model->getPerDay($tgl);
            $dt = $dataTgl[0]['rata'];
            $dataHarian[] = $dt;
          }
          echo "<pre>";
          // print_r($dataHarian);
          echo "</pre>";
        ?>

        <?php //echo "<p>Rata rata untuk tanggal ".$tgl." secara keseluruhan: ".$dataHarian[0]['rata'].".</p>"; ?>
        


        <?php


        ini_set('max_execution_time', 0); 
        ini_set('memory_limit','2048M');

         // $dir = "data/";
         // $files = scandir($dir);
         // print_r($files);

            // for($i=0;$i<count($files);$i++){
            //     if(substr($files[$i],0,4)=='2019'){
            //         $arrayTanggal[$i] = $files[$i];
            //     } else {
            //         $arrayTanggal[$i] = "tidak ada data";
            //     }
            // }
                    // var_dump($arrayTanggal);    

          //   $arrayTanggal[0]= $arrayTanggal[2];
            // unset($arrayTanggal[0]);
          //   $arrayTanggal[2]=null;
          //   $arrayTanggal[1]= $arrayTanggal[3];
            // unset($arrayTanggal[1]);
          //   $arrayTanggal[3] = null;


            for ($j=2; $j < sizeof($files); $j++) { 
            
            // print_r($arrayTanggal[$j]);
               $no = 0; $jumlah = 0;
               for ($i=7; $i <= 17; $i++) { 
                    $data[$j][$no] = $this->Gerak_model->getDataByTime($files[$j],$i);
                    // $data = $this->Gerak_model->getDataByTime($arrayTanggal[$j],$i);
                    // echo "tgl ".$files[$j]." jam ".$i." : ".$data[$j][$no]."<br>";
                    // print_r($data); echo "<br>";
                    $jumlah += $data[$j][$no];
                    $no++;    
                } 
                $sum[$j] = $jumlah;
          }
          // print_r($data[5]);
        ?>



      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <?php $this->load->view("admin/_partials/footer.php") ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->


  <?php $this->load->view("admin/_partials/scrolltop.php") ?>
  <?php $this->load->view("admin/_partials/modal.php") ?>

  <?php $this->load->view("admin/_partials/js.php") ?>

  <!-- Grafik Chart JS -->
  <script>
    var ajaxdata=[
      
    ];


    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php for($j=0; $j<count($tglData); $j++){ echo "'".$tglData[$j]."', "; } ?>],
            // labels: ['2019-08-05', '2019-08-07', '2019-08-10', '2019-08-12', '2019-08-14', '2019-08-17', '2019-08-19', '2019-08-21', '2019-08-24'],
            datasets: [
              {
                label: 'Monitoring Sensor Gas',
                data: [<?php for($j=0; $j<count($dataHarian); $j++){ echo $dataHarian[$j].", "; } ?>],
                backgroundColor: [
                    'rgb(40, 178, 170, 0.2)',
                    'rgb(40, 178, 170, 0.2)',
                    'rgb(40, 178, 170, 0.2)',
                    'rgb(40, 178, 170, 0.2)',
                    'rgb(40, 178, 170, 0.2)',
                    'rgb(40, 178, 170, 0.2)',
                    'rgb(40, 178, 170, 0.2)',
                    'rgb(40, 178, 170, 0.2)',
                    'rgb(40, 178, 170, 0.2)',
                ],
                borderColor: [
                    'rgba(135, 206, 250, 1)',
                    'rgba(135, 206, 250, 1)',
                    'rgba(135, 206, 250, 1)',
                    'rgba(135, 206, 250, 1)',
                    'rgba(135, 206, 250, 1)',
                    'rgba(135, 206, 250, 1)',
                    'rgba(135, 206, 250, 1)',
                    'rgba(135, 206, 250, 1)',
                    'rgba(135, 206, 250, 1)',
                ],
                borderWidth: 1
            },
            {
                label: 'Monitoring Gerakan (x100)',
                data: [<?php for ($i=0; $i<count($sum); $i++) { echo ($sum[$i+2]/100).", "; } ?>],
                backgroundColor: [
                    '#B0C4DE',
                    '#B0C4DE',
                    '#B0C4DE',
                    '#B0C4DE',
                    '#B0C4DE',
                    '#B0C4DE',
                    '#B0C4DE',
                    '#B0C4DE',
                    '#B0C4DE',
                ],
                borderColor: [
                    '#00FFFF',
                    '#00FFFF',
                    '#00FFFF',
                    '#00FFFF',
                    '#00FFFF',
                    '#00FFFF',
                    '#00FFFF',
                    '#00FFFF',
                    '#00FFFF',
                ],
                borderWidth: 1
            },
            ],
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });

      //ajax

  </script>

</body>

</html>


