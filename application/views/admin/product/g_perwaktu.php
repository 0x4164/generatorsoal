<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

  <?php $this->load->view("admin/_partials/navbar.php") ?>
  <div id="wrapper">

    <?php $this->load->view("admin/_partials/sidebar.php") ?>

    <div id="content-wrapper">

      <div class="container-fluid">

        <?php $this->load->view("admin/_partials/breadcrumb.php") ?>

        <!-- Letak Grafik -->

        <h4>Grafik Monitoring Kepadatan & Tingkat Polusi Per Waktu</h4>
        <ul>
          <?php
          //dari controller
          foreach($ls as $d){
            ?>
            <li>
            <a href="<?php echo base_url().'index.php/grafik/'.$d; ?>">
              Ke tanggal <?php echo $d; ?>
              </a>
            </li>
            <?php
          }
          ?>
        </ul>

        <canvas id="myChart"></canvas>
        <?php
        // echo $statistik;
        ?>
        <table class="table">
          <thead>
            <tr>
              <th rowspan="2">Nilai</th>
              <th colspan="3">Rata-rata Gas</th>
              <th colspan="3">Jumlah Gerak</th>
            </tr>
            <tr>
              <th>07-08</th>
              <th>11-12</th>
              <th>16-17</th>
              <th>07-08</th>
              <th>11-12</th>
              <th>16-17</th>
            </tr>
          </thead>
          <tbody>
          <?php
          // var_dump($statistik);
          foreach($statistik as $s){
            ?>
            <tr>
              <td><?php echo $s->nama;?></td>
              <td><?php echo $s->dgaPagi;?></td>
              <td><?php echo $s->dgaSiang;?></td>
              <td><?php echo $s->dgaSore;?></td>
              <td><?php echo $s->dgePagi;?></td>
              <td><?php echo $s->dgeSiang;?></td>
              <td><?php echo $s->dgeSore;?></td>
            </tr>
            <?php
          }
          ?>
          </tbody>
        </table>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <?php $this->load->view("admin/_partials/footer.php") ?>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->


  <?php $this->load->view("admin/_partials/scrolltop.php") ?>
  <?php $this->load->view("admin/_partials/modal.php") ?>

  <?php $this->load->view("admin/_partials/js.php") ?>

  <!-- Grafik Chart JS -->
  <script>

    // var ctx = document.getElementById('myChart').getContext('2d');
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart;

    function initChart(pctx,pdata){
      myChart = new Chart(pctx, {
          type: 'bar',
          data: pdata,
          options: {
              animation:false,
              scales: {
                  yAxes: [{
                      ticks: {
                          // beginAtZero: true
                      }
                  }]
              }
          }
        });
    }

      //ajax
      function getData(){
        console.log("get")
        $.ajax({
            type: 'GET',
            url: baseurl+'index.php/api/getDataWaktu',
            data: {
              tanggal:'<?php echo $tgl;?>'
            },
            success: function(data) {
              // json = JSON.parse(data);
              json=data
              // alert(json.msg);
              // myChart.data=json
              initChart(ctx,data)
              console.log(json)
            }
        });
      }
      getData()

      setInterval(getData, 10000);
  </script>

</body>

</html>


